export class Light {
  name: string;
  color: string;
  time: number;
  order: number;
  renderOrder: number;
}
