import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TlightComponent } from './tlight.component';
import { TlightService } from '../services/tlight.service';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { fakeAsync, tick, discardPeriodicTasks } from '@angular/core/testing';

const RED_RGB = 'rgb(255, 0, 0)';
const GREEN_RGB = 'rgb(0, 128, 0)';
const YELLOW_RGB = 'rgb(255, 255, 0)';

describe('TlightComponent', () => {
  let component: TlightComponent;
  let fixture: ComponentFixture<TlightComponent>;
  let tlightService: TlightService;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TlightComponent
      ],
      providers: [
        TlightService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TlightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    tlightService = TestBed.get(TlightService);
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it("should display all 3 lights", () => {
    fixture.detectChanges();

    let des: DebugElement[];

    des = fixture.debugElement.queryAll(By.css('circle'));

    expect(des.length).toBe(3);
  });


  it("should render red yellow and green in this particular order", fakeAsync(() => {
    let de: DebugElement;
    let el: HTMLElement;

    component.init();

    tlightService.getOrderedLights().forEach(function(light) {
      fixture.detectChanges();

      de = fixture.debugElement.query(By.css('.active'));
      el = de.nativeElement;

      switch (light.name) {
        case 'red':
          expect(el.style.fill).toBe(RED_RGB);
          break;
        case 'green':
          expect(el.style.fill).toBe(GREEN_RGB);
          break;
        case 'yellow':
          expect(el.style.fill).toBe(YELLOW_RGB);
          break;

      }

      tick(light.time*1000);
    })

    discardPeriodicTasks();

  }));


  it("should start with red", fakeAsync(() => {
    let de: DebugElement;
    let el: HTMLElement;

    component.init();

    fixture.detectChanges();

    de = fixture.debugElement.query(By.css('.active'));
    el = de.nativeElement;

    expect(el.style.fill).toBe(RED_RGB);

    discardPeriodicTasks();
  }));


  it("should switch to green after 3 sec", fakeAsync(() => {
    let de: DebugElement;
    let el: HTMLElement;

    component.init();

    tick(3000);
    fixture.detectChanges();

    de = fixture.debugElement.query(By.css('.active'));
    el = de.nativeElement;

    expect(el.style.fill).toBe(GREEN_RGB);

    discardPeriodicTasks();
  }));


  it("should switch to yellow after 5 sec", fakeAsync(() => {
    let de: DebugElement;
    let el: HTMLElement;

    component.init();

    tick(5000);
    fixture.detectChanges();

    de = fixture.debugElement.query(By.css('.active'));
    el = de.nativeElement;

    expect(el.style.fill).toBe(YELLOW_RGB);

    discardPeriodicTasks();
  }));


  it("should turn on one light at a time", fakeAsync(() => {
    let des: DebugElement[];
    let el: HTMLElement;
    let i: number;

    component.init();

    for (i=1; i<=10; i++) {
      fixture.detectChanges();

      des = fixture.debugElement.queryAll(By.css('.active'));

      expect(des.length).toBe(1);

      tick(1000);
    }

    discardPeriodicTasks();
  }));

});
