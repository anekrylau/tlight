import { Component, OnInit } from '@angular/core';
import { TlightService } from '../services/tlight.service';
import { Light } from '../shared/light';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-tlight',
  templateUrl: './tlight.component.html',
  styleUrls: ['./tlight.component.css']
})
export class TlightComponent implements OnInit {

  curLight: Light;
  lights: Light[];

  private timerSub: Subscription;

  constructor(private tlightService: TlightService) {
    this.lights = this.tlightService.getSortedLights();
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.curLight = this.tlightService.curLight;
    this.timerSub = this.tlightService.getLight(1000).subscribe(
      curLight => {
        this.curLight = curLight
      }
    )
  }

  reset() {
    this.tlightService.resetTlight();
    this.timerSub.unsubscribe();
    this.init();
  }

  getCurLight() {
    return this.tlightService.curLight;
  }

  getLightByOrder(order) {
    return this.tlightService.lights.filter(light => light.order == order)[0];
  }

}
