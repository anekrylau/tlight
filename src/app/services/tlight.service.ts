import { Injectable } from '@angular/core';
import { Observable, interval } from 'rxjs';
import { map } from 'rxjs/operators';

import { Light } from '../shared/light';


@Injectable({
  providedIn: 'root'
})
export class TlightService {

  lights: Light[] = [
    {
      name: 'red',
      color: '#ff0000', //red
      time: 3,
      order: 0,
      renderOrder: 0
    },
    {
      name: 'green',
      color: '#008000', //green
      time: 2,
      order: 1,
      renderOrder: 2
    },
    {
      name: 'yellow',
      color: '#ffff00', //yellow
      time: 1,
      order: 2,
      renderOrder: 1
    }
  ];

  curLight: Light = null;

  constructor() {
    this.curLight = this.getFirstLightByOrder();
  }

  lightTime: number = 0;

  getSortedLights(): Light[] {
    return this.lights.sort((x,y) => (x.renderOrder > y.renderOrder) ? 1 : ((y.renderOrder > x.renderOrder) ? -1 : 0));
  }

  getFirstLightByOrder(): Light {
    return this.getOrderedLights()[0];
  }

  getOrderedLights(): Light[] {
    return this.lights.sort((x,y) => (x.order > y.order) ? 1 : ((y.order > x.order) ? -1 : 0));
  }

  getLight(i): Observable<Light> {
    return interval(i)
    .pipe(
      map(() => {

        this.lightTime++;

        if (this.lightTime == this.curLight.time) {
          if (this.lights.filter(light => light.order == this.curLight.order+1)[0]) {
            this.curLight = this.lights.filter(light => light.order == this.curLight.order+1)[0];
          } else {
            this.curLight = this.lights.filter(light => light.order == 0)[0];
          }

          this.lightTime = 0;
        }

        return this.curLight;
      })
    );
  }

  resetTlight() {
    this.curLight = this.lights.filter(light => light.order == 0)[0];
    this.lightTime = 0;
  }
}
