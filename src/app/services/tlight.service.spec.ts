import { TestBed } from '@angular/core/testing';
import { TlightService } from './tlight.service';
import { fakeAsync, tick, discardPeriodicTasks } from '@angular/core/testing';

describe('TlightService', () => {

  let service: TlightService;

  beforeEach(() =>  {
    TestBed.configureTestingModule({
      providers: [
        TlightService
      ]
    })
    .compileComponents();

      service = TestBed.get(TlightService);
    }
  );

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('should select the very first light by order', () => {
    expect(service.curLight).toBe(service.getFirstLightByOrder());
  });


  it("should switch lights", fakeAsync(() => {
    service.getLight(1000).subscribe();

    service.lights.forEach(function(light) {
      expect(service.curLight.name).toBe(light.name);
      tick(light.time*1000);
    })

    discardPeriodicTasks();
  }));


  it('should reset correctly', fakeAsync(() => {
    service.getLight(1000).subscribe();

    tick(4000); // waiting a few seconds for the traffic light to change before we do the reset
    service.resetTlight();
    expect(service.curLight.name).toBe('red');

    discardPeriodicTasks();
  }));

});
